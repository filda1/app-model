import { StatusBar } from 'expo-status-bar';
import React, {useEffect} from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AsyncStorage from '@react-native-community/async-storage';
import keys from './constants/Keys';
import Parse from "parse/react-native.js";

import LandingScreen from './components/auth/Landing'
import RegisterScreen from './components/auth/Register'
import LoginScreen from './components/auth/Login'


Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(keys.applicationId, keys.javascriptKey);
Parse.serverURL = keys.serverURL;

const Stack = createStackNavigator()


function App() {
   

  useEffect(() => {
   /* const createInstallation = async () => {
      const Installation = Parse.Object.extend(Parse.Installation);
      const installation = new Installation();
  
      installation.set("deviceType", Platform.OS);
  
      await installation.save();
    }

   createInstallation();*/

   /* async function createUser(){
      const user = new Parse.User()
      user.set("username","test4")
      user.set("password","123456")
      user.set("email", "test4@gmail.com")
      user.set("telephone", 123456)

      
      try {
        await user.signUp()
        alert("User created")
      } catch (error) {
        alert(`ERROR: ${error.message}`)
        
      }
    }
   
    createUser()*/

  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Landing">
        <Stack.Screen name="Landing" component={LandingScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )

}

export default App
