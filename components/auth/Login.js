import React, { useState }  from 'react'
import { StyleSheet, Text, View, Button, TextInput } from 'react-native'


export default function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');


    const onSignUp = () => {
     
            console.log("Oka")
    }

    return (
        <View>
            <TextInput
                placeholder="email"
                onChangeText={(email) => setEmail({ email })}
            />
            <TextInput
                placeholder="password"
                secureTextEntry={true}
                onChangeText={(password) => setPassword({ password })}
            />

            <Button
                onPress={onSignUp}
                title="Sign In"
            />
        </View>
    )
}

const styles = StyleSheet.create({})
