import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native'


export default function Landing({ navigation}) {
    return (
        <View style={styles.container}>
         <TouchableOpacity   
           style={styles.button}    
           onPress={ () => navigation.navigate("Register")} >
              <Text>Register</Text>
          </TouchableOpacity>
           
          <TouchableOpacity
             style={styles.button}
             onPress={ () => navigation.navigate("Login")}>
             <Text>Login</Text>
          </TouchableOpacity>
     
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
      }
})

